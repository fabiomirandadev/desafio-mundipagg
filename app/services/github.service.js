class GithubService {
  constructor ($http) {
    'ngInject';
    this._$http = $http
    this.baseURL = 'https://api.github.com/'
  }
  getRepositories () {
    return this._$http.get(`${this.baseURL}users/mundipagg/repos`)
  }

  getRepositoryCommits (name) {
    return this._$http.get(`${this.baseURL}repos/mundipagg/${name}/commits`)
  }

  getContributors (repositoryName) {
    return this._$http.get(`${this.baseURL}repos/mundipagg/${repositoryName}/contributors`)
  }

}

GithubService.$inject = ['$http']

export { GithubService }
