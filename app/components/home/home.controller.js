import './home.view.html'

class HomeController {
  constructor (GithubService, moment) {
    'ngInject';
    this._GithubService = GithubService
    this._moment = moment
    this.repositories = {}
    this.repositorySelected = {
      stargazers_count: 0,
      forks_count: 0
    }

    this.canvas = {}
    this.canvas.colors = ['#082659']
    this.canvas.labels = []
    this.canvas.data = []
    this.canvas.datasetOverride = [{
      label: 'commits',
      type: 'line',
      borderWidth: 3
    }]

    this.loadRepostories()
  }

  loadRepostories () {
    this._GithubService.getRepositories()
      .then(res => {
        this.repositories = res.data
      })
  }

  loadContributors (repositoryName) {
    this._GithubService.getContributors(repositoryName)
      .then(res => {
        let contributors = res.data
        this.contributorsNumber = contributors.length
      })
  }
  loadCommitsData (repositoryName) {
    this._GithubService.getRepositoryCommits(repositoryName)
      .then(res => {
        this.canvas.labels = []
        this.canvas.data = []

        let numberCommits = 0
        res.data.map((item, index) => {
          let date = this._moment(item.commit.committer.date , 'YYYY/MM/DD')
          if (!this.canvas.labels.includes(date.format('DD/MMMM/YYYY'))) {
            if (numberCommits > 0)
              this.canvas.data.unshift(numberCommits)

            this.canvas.labels.unshift(date.format('DD/MMMM/YYYY'))
            numberCommits = 1
          } else
            numberCommits++
        })
        this.canvas.data.unshift(numberCommits)
        this.canvas.data.push(0) // Para o Gráfico não acabar estranho
      })
  }

  loadInfosRepo () {
    this.loadContributors(this.repositorySelected.name)
    this.loadCommitsData(this.repositorySelected.name)
  }

}

HomeController.$inject = ['GithubService', 'moment']

export { HomeController }
