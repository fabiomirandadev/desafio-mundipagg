import { Router } from './router.js'
import { HomeController } from './components/home/home.controller.js'
import { GithubService } from './services/github.service.js'

let app = angular.module('App', ['ui.router', 'chart.js', 'angularMoment'])

Router.configure(app)

app
  .controller('HomeController', HomeController)
  .service('GithubService', GithubService)
